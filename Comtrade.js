/*global require*/
/*
 * Bootstrap-based responsive mashup
 * @owner Erik Wetterberg (ewg)
 */
/*
 *    Fill in host and port for Qlik engine
 */
var prefix = window.location.pathname.substr( 0, window.location.pathname.toLowerCase().lastIndexOf( "/extensions" ) + 1 );

var config = {
	host: 'viz.dev.un.org',
	prefix: "/visualization/",
	port: null,
	isSecure: true
};
//to avoid errors in workbench: you can remove this when you have added an app
var app;
require.config( {
	baseUrl: (config.isSecure ? "https://" : "http://" ) + config.host + (config.port ? ":" + config.port : "" ) + config.prefix + "resources"
} );

require( ["js/qlik"], function ( qlik ) {

	/* display error message (have some bug to fix)
    $("#closeerr").on('click',function(){
		$("#errmsg").html("").parent().hide();
	});
	qlik.setOnError( function ( error ) {
		$("#errmsg").append("<div>"+error.message+"</div>").parent().show();
	} );*/
    
    
    $( "html" ).removeClass( "loading" ); // loading animation: remove the loading class after the page loaded

	//
	function AppUi ( app ) {
		var me = this;
		this.app = app;
		app.global.isPersonalMode( function ( reply ) {
			me.isPersonalMode = reply.qReturn;
		} );
		app.getAppLayout( function ( layout ) {
			$( "#title" ).html( layout.qTitle );
			$( "#title" ).attr("title", "Last reload:" + layout.qLastReloadTime.replace( /T/, ' ' ).replace( /Z/, ' ' ) );
			//TODO: bootstrap tooltip ??
		} );
		app.getList( 'SelectionObject', function ( reply ) {
			$( "[data-qcmd='back']" ).parent().toggleClass( 'disabled', reply.qSelectionObject.qBackCount < 1 );
			$( "[data-qcmd='forward']" ).parent().toggleClass( 'disabled', reply.qSelectionObject.qForwardCount < 1 );
		} );
		app.getList( "BookmarkList", function ( reply ) {
			var str = "";
			reply.qBookmarkList.qItems.forEach( function ( value ) {
				if ( value.qData.title ) {
					str += '<li><a href="#" data-id="' + value.qInfo.qId + '">' + value.qData.title + '</a></li>';
				}
			} );
			str += '<li><a href="#" data-cmd="create">Create</a></li>';
			$( '#qbmlist' ).html( str ).find( 'a' ).on( 'click', function () {
				var id = $( this ).data( 'id' );
				if ( id ) {
					app.bookmark.apply( id );
				} else {
					var cmd = $( this ).data( 'cmd' );
					if ( cmd === "create" ) {
						$('#createBmModal' ).modal();
					}
				}
			} );
		} );
		$( "[data-qcmd]" ).on( 'click', function () {
			var $element = $( this );
			switch ( $element.data( 'qcmd' ) ) {
				//app level commands
				case 'clearAll':
					app.clearAll();
					break;
				case 'back':
					app.back();
					break;
				case 'forward':
					app.forward();
					break;
				case 'lockAll':
					app.lockAll();
					break;
				case 'unlockAll':
					app.unlockAll();
					break;
				case 'createBm':
					var title = $("#bmtitle" ).val(), desc = $("#bmdesc" ).val();
					app.bookmark.create( title, desc );
					$('#createBmModal' ).modal('hide');
					break;
				case 'reload':
					if ( me.isPersonalMode ) {
						app.doReload().then( function () {
							app.doSave();
						} );
					} else {
						qlik.callRepository( '/qrs/app/' + app.id + '/reload', 'POST' ).success( function ( reply ) {
							//TODO:handle errors, remove alert
							alert( "App reloaded" );
						} );
					}
					break;
			}
		} );
        
	}
	//callbacks -- inserted here --
	//open apps -- inserted here --
	var app = qlik.openApp('bac203f6-e4b3-47b5-9d68-0a73ff837f4c', config);


	//get objects -- inserted here --
	app.getObject('value-kpi','CGErJX');
	app.getObject('data-gauge','bJxyxpU');
	app.getObject('growth-line','efjtGkS');
	app.getObject('main-flow','Lrtp');
	app.getObject('trade-selection','tPGryJ');
	app.getObject('icon','xPGHkt');
	app.getObject('country-map','VPrsZ');
	app.getObject('region-filter','qJUGafd');
	app.getObject('country-filter','yDPGyx');
	app.getObject('commodity-filter','eZjRM');
	app.getObject('commodities-tree','YynkYB');
	app.getObject('destination-bar','crGXXDj');
    app.getObject('net-bar','LZjSku');
	app.getObject('records-kpi','HmJhRVJ');
	app.getObject('countries-kpi','pAbkf');
	app.getObject('year-api','jQfByz');
	app.getObject('commodities-kpi','XKBEyD');
	app.getObject('year-bar','HzXNT');
	app.getObject('year-slider','FGeArrZ');
    app.getObject('version-kpi','jBAAJw');
	//create cubes and lists -- inserted here --
	if(app) {
		new AppUi( app );
	}

} );

